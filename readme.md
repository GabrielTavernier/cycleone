# Readme

## Précision sur les icônes
    
J'ai fait le choix d'utiliser des sprites SVG. Toutes mes icônes sont ainsi regroupées dans un seul fichier, réparties sur des claques nommées.\
L'avantage est qu'il n'y a qu'un seul fichier à appeler et une fois en cache on a accès à toutes les icones du set. Le fichier reste léger (12ko pour 32 icones) et il est très facile d'en modifier la couleur ou la taille du trait depuis la feuille de style. 
    
Le gros inconvénient c'est qu'en local les navigateurs considèrent ces fichiers comme des ressources externes et en bloque l'accès.\
On peut contourner ce blocage facilement : 
- Pour Firefox il faut sur rendre dans les paramètres du navigateur sur la page *about:config* et désactiver la propriété "privacy.file_unique_origin" : [solution pour Firefox](https://discourse.mozilla.org/t/firefox-68-local-files-now-treated-as-cross-origin-1558299/42493)
- Pour Chrome (Mac OS) il faut fermer Chrome et lancer cette commande dans le terminal `open /Applications/Google\ Chrome.app --args --allow-file-access-from-files` : [solution pour Chrome MacOS](http://blog.derraab.com/2013/05/30/start-google-chrome-with-local-file-access-on-mac-os-x/#main)
- Pour Chrome (Windows) ouvrez le terminal et rendez-vous dans le dossier où Chrome est installé (généralement C:\Program Files\Google\Chrome\Application\) et lancer la commande `.\chrome.exe --allow-file-access-from-files` : [solution pour Chrome Windows](https://stackoverflow.com/questions/18586921/how-to-launch-html-using-chrome-at-allow-file-access-from-files-mode)

## En ligne
Le problème ne se pose pas une fois le site en ligne donc vous pouvez consulter mon projet depuis mon serveur : [Cycle One](http://cycle-one.gabrieltavernier.fr/)\
**Petit bug en ligne :** la carte google maps ne se charge pas sur Firefox Mobile. Je pense que c'est un problème de sécurité, mon domaine ne possédant pas de certificat SSL.

