function navMobile() {
    $('a.btn-toggle-menu, .mobile-nav .main-nav').removeClass('on'); // permet de masquer le menu au lancement du site
    $('a.btn-toggle-menu').on('click', function(event) {
        event.preventDefault();
        $(this).toggleClass('on').blur();
        $('.mobile-nav .main-nav').toggleClass('on');
    });
}

function accordion() {
    $('.accordion-item .accordion-item-content').slideUp();

    $('.accordion-item').on('click', function(){
        if ($(this).hasClass('on')) {
            $(this).removeClass('on');
            $(this).find('.accordion-item-content').slideUp();
        } else {
            $('.accordion-item').removeClass('on');
            $(this).addClass('on');
            $('.accordion-item .accordion-item-content').slideUp();
            $('.accordion-item.on .accordion-item-content').slideDown();
        }
    });
}

function configQuantity() {
    $('.product-configuration-quantity button').on('click', function(e){
        e.preventDefault();
        
        let inputValue = $(this).parent().find("input").val();
        let buttonValue = $(this).text();
        
        if(buttonValue === '+') {
            if (inputValue < 10) {
                inputValue++;
            }
        }
        if(buttonValue === '-') {
            if (inputValue > 1) {
                inputValue--;
            }
        }

        $(this).parent().find("input").val(inputValue);
    });

    $('.product-configuration-quantity input').on('change', function(){
            if($(this).val() > 10) {
                $(this).val(10);
            };
            if($(this).val() < 1) {
                $(this).val(1);
            };
    });
}

function configColor() {
    if ($('.product-configuration-color ul.color-list input').is(':checked')) {
        $('.product-configuration-color ul.color-list li label').addClass('off').removeClass('on');
        $('.product-configuration-color ul.color-list input:checked').parent('label').removeClass('off').addClass('on');

        $('.product-configuration .form-button button').removeAttr('disabled');
    }

    $('.product-configuration-color ul.color-list li label').on('change', function(){
        $('.product-configuration-color ul.color-list li label').addClass('off').removeClass('on');
        $(this).removeClass('off').addClass('on');

        let color = $(this).find('input').attr('title');
        console.log('couleur : ' + color);
        let mainPicture = 'img/produit/casque-' + color + '.jpg';
        let mediumPicture = 'img/produit/casque-' + color + '-600px.jpg';
        $('.product-main-picture a').attr('href', mainPicture);
        $('.product-main-picture img').attr('src', mediumPicture);

        $('.product-configuration .form-button button').removeAttr('disabled');
    });
}

function announcement() {
    $('.announcement-list').slick({
        centerMode: true,
        slidesToShow: 1,
        adaptiveHeight: true
    });
}

function pressReview() {
    $('.press-review ul').slick({
        slidesToShow: 3,
        infinite: true,
        centerMode: true,
        variableWidth: true
    });
}

function secondaryPictures() {
    $('.product-secondary-picture').slick({
        centerMode: false,
        slidesToShow: 4 ,
        adaptiveHeight: false
    });
}

function fancyBox() {
    $('[data-fancybox="gallery"], [data-fancybox="mainPicture"]').fancybox({
        buttons : ['zoom','fullScreen', 'close'],
        animationEffect: false,
        transitionEffect: "slide",
        loop: true,
    });
}

function messenger() {
    let alreadyClose = false;
    
    $(window).scroll(function() {
        if($(window).scrollTop() + $(window).height() > $(document).height() - 100 && alreadyClose === false) {
                $('aside.messenger .tooltip').show();
        }
    });

    $('aside.messenger .tooltip-close').on('click', function(){
        $('aside.messenger .tooltip').hide();
        alreadyClose = true;
    });
}