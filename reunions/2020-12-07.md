Squiffiec, le 7 décembre 2020

# Compte rendu de réunion

## Objet : Réunion de chantier sur l'avancement du projet Cycle One
Lieu : Squiffiec\
Date : 07/12/2020\
Auteur : Gabriel TAVERNIER

----

### Participants
#### présent(s) :
- Gabriel TAVERNIER
#### excusé(s) : 
- aucun
#### absent(s) : 
- aucun

---

## Ordre du jour 
Faire un bilan sur ce qui a été fait la semaine précédente, visualiser ce qu'il reste à faire et définir les priorités pour la semaine.

### Ce qui a été fait la semaine dernière
- Le style guide est terminé
- La mise en page CSS de l'ensemble des pages (en particulier de la page produit) est terminée
- Le style graphique général a été harmonisé en suivant le style guide
- Un rapide redécoupage des feuilles de style a été effectué
- Une optimisation du code JS a également été faite
- Quelques visuels ont été créé / ajouté

### Ce qu'il reste à faire
- Rédiger le rapport final
- Si possible trouver ou créer quelques visuels d'interface démontrant l'application mobile

### Ce qui a posé problème
- Manque de temps et nouveau projet qui s'accumule (Symphony, Wordpress, Anglais...)

### Ce qu'on fera en priorité cette semaine 
- Rédaction du rapport final

### Ce qu'il restera à faire pour les semaines suivantes
 - Plus rien le projet sera livré

----